//
//  GithubContributor.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import Foundation

struct GithubContributor {
    let login: String
    let id: Int
    let avatarURL: URL?
    
    init(response: GithubContributorResponseItemResponse) {
        self.login = response.login
        self.id = response.id
        self.avatarURL = URL(string: response.avatarURL)
    }
}
