//
//  UserListViewController.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import UIKit

class UserListViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var usersTableView: UITableView!
    
    // MARK: - State
    private let apiProvider: ApiProvider
    private var githubContributors = [GithubContributor]() {
        didSet {
            usersTableView.reloadData()
        }
    }

    // MARK: - Life Cycle
    internal init(apiProvider: ApiProvider) {
        self.apiProvider = apiProvider
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        visibleLoader(true)
        setupViews()
        loadGithubContributors()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup Views
    private func setupViews() {
        title = "Contributors"
        setupTableView()
    }
    
    private func setupTableView() {
        usersTableView.delegate = self
        usersTableView.dataSource = self
        usersTableView.tableFooterView = UIView()
        usersTableView.rowHeight = 70
        usersTableView.register(
            UINib(
                nibName: GithubContributorCell.reusableIdentifier,
                bundle: nil
            ),
            forCellReuseIdentifier: GithubContributorCell.reusableIdentifier
        )
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        
        usersTableView.refreshControl = refreshControl
    }
}

// MARK: - Logic
extension UserListViewController {
    private func loadGithubContributors() {
        githubContributors = []
        apiProvider.getContributors { [weak self] result in
            
            self?.usersTableView.refreshControl?.endRefreshing()
            self?.visibleLoader(false)
            
            switch result {
            
            case .success(let contributors):
                self?.githubContributors = contributors
                
            case .failure(let apiError):
                debugPrint("API ERROR ❌ -> \(apiError.errorMessage)")
                self?.showInfoAlert(title: "ERROR ❌", description: apiError.errorMessage)
            }
        }
    }
    
    @objc
    private func handleRefreshControl() {
        loadGithubContributors()
    }
    
    private func routeToUserDetailsScreen(githubContributor: GithubContributor) {
        let userDetailsViewController = UserDetailsViewController(githubContributor: githubContributor)
        navigationController?.pushViewController(userDetailsViewController, animated: true)
    }
}

// MARK: - UITableViewDelegate
extension UserListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let githubContributor = githubContributors[indexPath.row]
        routeToUserDetailsScreen(githubContributor: githubContributor)
    }
}

// MARK: - UITableViewDataSource
extension UserListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return githubContributors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = usersTableView.dequeueReusableCell(
                withIdentifier: GithubContributorCell.reusableIdentifier,
                for: indexPath) as? GithubContributorCell else {
            return .init()
        }
        
        let model = githubContributors[indexPath.row]
        cell.configure(model: model)
        
        return cell
    }
}
