//
//  UserDetailsViewController.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import UIKit

class UserDetailsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    
    // MARK: - State
    private let githubContributor: GithubContributor
    
    // MARK: - Life Cycle
    internal init(githubContributor: GithubContributor) {
        self.githubContributor = githubContributor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    // MARK: - Setup Views
    private func setupViews() {
        if let avatarURL = githubContributor.avatarURL {
            avatarImageView.loadByURL(avatarURL)
        }
        loginLabel.text = githubContributor.login
    }
}
