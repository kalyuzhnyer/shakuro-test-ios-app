//
//  ApiProvider.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import Foundation

typealias GetContributorsCompletion = (Result<[GithubContributor], ApiError>) -> Void

protocol ApiProvider {
    func getContributors(completion: @escaping GetContributorsCompletion)
}

class ApiProviderImpl {
    private let session = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask?
}

// MARK: - ApiProvider
extension ApiProviderImpl: ApiProvider {
    func getContributors(completion: @escaping GetContributorsCompletion) {
        guard let requestURL = URL(string: "https://api.github.com/repos/videolan/vlc/contributors") else {
            completion(.failure(.invalidRequestURL))
            return
        }
        
        dataTask?.cancel()
        
        dataTask = session.dataTask(with: requestURL) { [weak self] data, response, error in
            defer {
                self?.dataTask = nil
            }
            
            DispatchQueue.main.async {
                if let error = error as NSError? {
                    if error.code == -1020 {
                        completion(.failure(.notInternetConnection))
                    } else {
                        completion(.failure(.unknown))
                    }
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    completion(.failure(.unknown))
                    return
                }
                
                if httpResponse.statusCode == 500 {
                    completion(.failure(.internalServer))
                    return
                }
                
                if httpResponse.statusCode == 404 {
                    completion(.failure(.notFound))
                    return
                }
                
                if httpResponse.statusCode != 200 {
                    completion(.failure(.unknown))
                    return
                }
            }
            
            guard let data = data,
                  let githubContributorsResponse = try? JSONDecoder().decode([GithubContributorResponseItemResponse].self, from: data) else {
                DispatchQueue.main.async {
                    completion(.failure(.decode))
                }
                return
            }
            
            let githubContributors: [GithubContributor] = githubContributorsResponse.map({ .init(response: $0) })
            
            DispatchQueue.main.async {
                completion(.success(githubContributors))
            }
        }
        
        dataTask?.resume()
    }
}
