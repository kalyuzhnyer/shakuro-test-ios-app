//
//  ApiError.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import Foundation

enum ApiError: Swift.Error {
    case notInternetConnection
    case internalServer
    case invalidRequestURL
    case notFound
    case unknown
    case decode
    
    var errorMessage: String {
        switch self {
        
        case .notInternetConnection:
            return "Bad internet connection :("
        case .internalServer:
            return "Internal server Error :("
            
        case .invalidRequestURL:
            return "Invalid request url :("
            
        case .notFound:
            return "Not found :("
            
        case .unknown:
            return "Unknown error :("
            
        case .decode:
            return "Decode error :("
        }
    }
}
