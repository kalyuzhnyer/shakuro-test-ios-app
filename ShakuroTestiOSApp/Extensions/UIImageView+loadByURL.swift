//
//  UIImageView+loadByURL.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import UIKit

extension UIImageView {
    func loadByURL(_ url: URL) {
        DispatchQueue.global().async { [weak self] in
            guard let imageData = try? Data(contentsOf: url),
                  let image = UIImage(data: imageData) else {
                return
            }
            
            DispatchQueue.main.async {
                self?.image = image
            }
        }
    }
}
