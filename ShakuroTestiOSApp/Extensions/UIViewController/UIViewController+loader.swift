//
//  UIViewController+loader.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import UIKit

fileprivate let loaderId = "shakuro_loader_view_id"
fileprivate let backdropId = "shakuro_loader_back_drop_view_id"

extension UIViewController {
    
    func visibleLoader(_ value: Bool) {
        if value {
            showLoader()
        } else {
            hideLoader()
        }
    }
    
    fileprivate func showLoader() {
        view.isUserInteractionEnabled = false
        showBackDrop()
        showIndicator()
    }
    
    fileprivate func hideLoader() {
        view.isUserInteractionEnabled = true
        hideIndicator()
        hideBackDrop()
    }
    
    fileprivate func showBackDrop() {
        let effect = UIBlurEffect(style: .dark)
        let backDropView = UIVisualEffectView(frame: UIScreen.main.bounds)
        backDropView.accessibilityIdentifier = backdropId
        backDropView.effect = effect
        backDropView.alpha = 0.8
        
        UIApplication.shared.keyWindow?.addSubview(backDropView)
    }
    
    fileprivate func hideBackDrop() {
        removeViewByIdentifier(backdropId)
    }
    
    fileprivate func showIndicator() {
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.accessibilityIdentifier = loaderId
        indicator.color = .red
        indicator.startAnimating()
        
        UIApplication.shared.keyWindow?.addSubview(indicator)
        indicator.center = UIApplication.shared.keyWindow?.center ?? .zero
    }
    
    fileprivate func hideIndicator() {
        removeViewByIdentifier(loaderId)
    }
    
    fileprivate func removeViewByIdentifier(_ identifier: String) {
        UIApplication.shared.keyWindow?
            .subviews
            .first(where: { $0.accessibilityIdentifier == identifier })?
            .removeFromSuperview()
    }
}
