//
//  UIViewController+Alert.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import UIKit

extension UIViewController {
    func showInfoAlert(title: String, description: String? = nil) {
        let alertVC = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertVC.addAction(okAction)
        
        present(alertVC, animated: true, completion: nil)
    }
}
