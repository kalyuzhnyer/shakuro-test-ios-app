//
//  GithubContributorCell.swift
//  ShakuroTestiOSApp
//
//  Created by Evgeny Kalyuzhny on 18.05.2021.
//

import UIKit

class GithubContributorCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    // MARK: - State
    var avatarURL: URL? {
        didSet {
            loadAvatar()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = nil
    }
    
    func configure(model: GithubContributor) {
        loginLabel.text = model.login
        idLabel.text = "\(model.id)"
        avatarURL = model.avatarURL
    }
    
    private func loadAvatar() {
        if let avatarURL = avatarURL {
            DispatchQueue.global().async { [weak self] in
                guard let imageData = try? Data(contentsOf: avatarURL),
                      let image = UIImage(data: imageData) else {
                    return
                }
                
                if self?.avatarURL == avatarURL {
                    DispatchQueue.main.async {
                        self?.avatarImageView.image = image
                    }
                }
            }
        }
    }
}
